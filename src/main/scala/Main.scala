import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}

import sys.process._

/**
  * Created by rohitash on 3/1/17.
  */
object Main {

  def main(args: Array[String]): Unit = {
   /* val epoch = args(0)  // expecting the epoch of the hour you want to reprocess
    val auctionView = args(1)

    val groupBySortHourValue = groupedSort.getMetricSumOverHour(epoch.toLong)

    def fun(x:Double, y: Double): Boolean = {
      val dif = x-y ;
      val abs = dif.abs ;
      if((abs/y)*100 >= 1) true ;
      else false ;
    }*/

   /* if(fun(auctionView.toDouble, groupBySortHourValue.toDouble)) {
      // mismatch of more then 1% between groupSort value and auctionView value

      groupedSort.reprocess(epoch.toLong)

      /*if(status == 0) {
        println("GroupedSort Jobs created Successfully")
      }
      else {
        println("Some error in GroupedSort reprocess script")
      }*/
    }
    else {
      // auctionView and groupSort values are consistent within 1%
      // need to consider wrangler
      val dt = new DateTime(epoch, DateTimeZone.forOffsetHours(0));
      val fmt = DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss");
      val date = fmt.print(dt);

      // comment the kill script

      val commentStatus = ("./CommentKillScript.sh").!

      if(commentStatus == 0) {
        println("commenting was successful")
      }
      else {
        println("commenting was not successful check ssh")
      }

      // get list of all keys from validator_files db

      val query = "select `key` from validator_files where file_time = '" + date + "'" + " and src_name = \"vdopia\" "
      val keys = Sql.findQuery(query, "key")
      println(keys)



      // delete all the entries with that key in validator_queue, wrangler_file, validator_files

      val query2 = "`key` in ('" + keys.mkString("','") + "')"
      val deleteValidatorQueue = "delete from validator_queue where " + query2
      val deleteWranglerFiles = "delete from wrangler_files where " + query2
      val deleteValidatorFile = "delete from validator_files where " + query2

      val validatorcountQuery = "select * from validator_queue where " + query2

      val validatorQueueCount = Sql.countQuery(validatorcountQuery)

      println("Deleting Validator_files entries")
      Sql.deleteQuery(deleteValidatorFile)

      println("Deleting Validator_Queue entries")
      Sql.deleteQuery(deleteValidatorQueue)

      println("Deleted Wrangler_files entries")
      Sql.deleteQuery(deleteWranglerFiles)

      // kill all the validators

      val status = ("./killValidator.sh ").!

      if(status == 0) {
        println("validator kill was successful")
      }
      else {
        println("validator kill was not successful")
      }

      // mark all the jobs with those keys in wrangler_queue for that hour to P state

      println("marking all jobs in wrangler queue To P state")
      val updateWranglerQueue = "update wrangler_queue set state = 'P' where " + query2

      Sql.updateQuery(updateWranglerQueue)

      // wait till all the jobs get to P state
      println("waiting for all jobs in wrangler queue to complete")
      val wranglerQueueQuery = "select count(*) from wrangler_queue where state = 'P' and " + query2
      var bool = true
      while(bool) {
        var count = Sql.countQuery(wranglerQueueQuery)
        if(count == 0) bool = false
        else Thread.sleep(10000)
      }

      // Poll till all the jobs are populated in validator_queue by DM

      while(validatorQueueCount != Sql.countQuery(validatorcountQuery)) Thread.sleep(10000)


      // eliminate different hour keys from validator_queue

      println("eliminating keys of different hours")

      val filterKeyQuery = "update validator_queue set state = 'C' where file_time != '" + date + "' and " + query2

      Sql.updateQuery(filterKeyQuery)


      // delete raw collection entry for that hour from mongo

      println("Deleting entry of Raw of the hour from mongo")
      RawCollection.deleteRawCollectionEntry(epoch.toLong, "Vdopia-Factual")

      // turn on validator

      val turnOnStatus = ("./turnOnValidator.sh ") !

      if(turnOnStatus != 0) {
        println("Error : Validators are not up ")
      }
      else {
        println("chill hai")
      }


      // uncomment the kill script

      val uncommentStatus = ("./UncommentKillScript").!
      if(uncommentStatus == 0) {
        println("Uncommenting was done successfully")
      }
      else {
        println("Uncommenting was not successful")
      }

     }
*/

  }


}

