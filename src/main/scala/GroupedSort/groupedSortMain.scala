package GroupedSort

import org.joda.time.{DateTime, DateTimeZone}
import reactivemongo.bson.BSONDocument
import org.apache.spark.sql.functions._
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Created by rohitash on 8/3/17.
  */
object groupedSortMain  extends App {

  val subSource = args(0)
  val year = args(1)
  val month = args(2)
  val day = args(3)
  val hour = args(4)

  val source = {
    if(subSource.contains("bid")) "Grouped-Vdopia-Sample-Bid-Factual"
    else "Grouped-Vdopia-Sample-Factual"
  }
  DateTimeZone.setDefault(DateTimeZone.UTC)
  val dt = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(0).getMillis
  println(dt)

  val query = BSONDocument(
    "time" -> dt,
    "key"  -> source
  )
  import Configs.MongoConnection._
  val fileResult = groupedSortCollection.find(query).cursor[BSONDocument].toList().map(x => {
    x.map(y => {
      val reads = reader.read(y)
      reads.file
    })
  })

  val out = Await.result(fileResult, 15 seconds)

  if(out.length == 0) println("either you got no groupedSort for that time or something is amiss")
  else print(out(0))

  import Configs.SparkConf._
  val df = sqlContext().read.parquet(out:_*)
  //df.cache()

  val auctions = df.groupBy().agg(sum("impressions_offered")).collect()(0).get(0)
  val advertiserRevenue  = df.groupBy().agg(sum("advertiserRevenue")).collect()(0).get(0)
  val publisherRevenue = df.groupBy().agg(sum("publisherRevenue")).collect()(0).get(0)

  println("following are the groupedSort count :")
  println("auctions/impressions  : " + auctions)
  println("advertiser revenue  : " + advertiserRevenue)
  println("publisher revenue  : " + publisherRevenue)

  System.exit(0)
}
