package S3

import org.apache.spark.sql.functions._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}


/**
  * Created by rohitash on 8/3/17.
  */

object s3Main extends App {

  val subSource = args(0)
  val year = args(1)
  val month = args(2)
  val day = args(3)
  val hour = args(4)

  val folderPath = "s3://sigmoid-vdopia/base-data/" + subSource + "/" + year + "/" + month + "/" + day + "/" + hour + "/*"
  println(folderPath)

  val t1 = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(0).getMillis.toString
  println(t1)

  val t2 = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(30).getMillis.toString
  println(t2)

  val halfHourTimestampFunc: (String => String) = (in: String) => {
    val date = DateTime.parse(in, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"))
    //val date = new DateTime(dateLong).withZone(DateTimeZone.UTC) //DateTime.parse(in, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"))
    date.minuteOfHour().get() match{
      case x if x<30 =>
        date.withZone(DateTimeZone.UTC).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
          .getMillis.toString
      case _ =>
        date.withZone(DateTimeZone.UTC).withMinuteOfHour(30).withSecondOfMinute(0).withMillisOfSecond(0)
          .getMillis.toString
    }
  }

  val halfHourTimestampSqlFunc = udf(halfHourTimestampFunc)
  import Configs.SparkConf._
  val data = sqlContext().read.json(folderPath)
  //data.cache()
  val hourDF = data.withColumn("hour", halfHourTimestampSqlFunc(data("timestamp")))
  val auctions = hourDF.filter("hour <= " + t2).filter("hour >= " + t1).agg(sum("exchangeInternal.samplingFactor")).collect

  println("auctions in json files of s3 bucket : " + auctions(0).get(0))

  System.exit(0)
}
