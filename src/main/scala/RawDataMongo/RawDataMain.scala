package RawDataMongo

import org.apache.spark.sql.functions._
import org.joda.time.{DateTime, DateTimeZone}
import reactivemongo.bson.BSONDocument

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
  * Created by rohitash on 8/3/17.
  */
object RawDataMain extends App {

  val subSource = args(0)
  val year = args(1)
  val month = args(2)
  val day = args(3)
  val hour = args(4)

  val source = {
    if(subSource.contains("bid")) "Vdopia-Sample-Bid-Factual"
    else "Vdopia-Sample-Factual"
  }

  DateTimeZone.setDefault(DateTimeZone.UTC)
  val dt = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(0).getMillis
  println(dt)

  val query = BSONDocument(
    "time" -> dt,
    "sourceName" -> source
  )

  import Configs.MongoConnection._
  val fileResult = rawCollection.find(query).cursor[BSONDocument].toList().map(x => {
    x.map(y => {
      y.getAs[String]("file").get
    })
  })

  println("waiting for future to return")
  val out = Await.result(fileResult, 5 seconds).flatMap(x => x.split(","))

  println(out.length)

  import Configs.SparkConf._
  val df = sqlContext().read.parquet(out:_*)
  //df.cache()
  val auctions = df.groupBy().agg(sum("impressions_offered")).collect()(0).get(0)
  val advertiserRevenue  = df.groupBy().agg(sum("advertiserRevenue")).collect()(0).get(0)
  val publisherRevenue = df.groupBy().agg(sum("publisherRevenue")).collect()(0).get(0)

  println("following are the mongo collection RawData count : ")
  println("auctions/impressions  : " + auctions)
  println("advertiser revenue  : " + advertiserRevenue)
  println("publisher revenue  : " + publisherRevenue)

  System.exit(0)

}
