package Wrangler

import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}
import org.apache.spark.sql.functions._

/**
  * Created by rohitash on 8/3/17.
  */
object wranglerMain extends App {

  val subSource = args(0)
  val year = args(1)
  val month = args(2)
  val day = args(3)
  val hour = args(4)

  val source = {
    if(args(0).contains("bid")) "vdopiasample-bid"
    else "vdopiasample"
  }

  val dt = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(0).getMillis

  val fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss") ;
  val date = fmt.print(dt)

  val query = "select * from wrangler_files where file_time = '" + date + "' and src_name = '" + source + "' ;"

  println(query)

  import Configs.Sql._
  val fileList = findQuery(query, "dst").flatMap(x => x.split(","))
  fileList.foreach(println(_))

  import Configs.SparkConf._
  val df = sqlContext().read.parquet(fileList :_*)
  //df.cache()
  var auctions = df.groupBy().agg(sum("impressions_offered")).collect()(0).get(0)
  val advertiserRevenue  = df.groupBy().agg(sum("advertiserRevenue")).collect()(0).get(0)
  val publisherRevenue = df.groupBy().agg(sum("publisherRevenue")).collect()(0).get(0)

  println("following are the wrangler counts : ")
  println("auctions : " + auctions)
  println("advertiser revenue : " + advertiserRevenue)
  println("publisher revenue  : " + publisherRevenue)

  System.exit(0)
}
