package Ingestor

import org.apache.spark.sql.functions._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}

/**
  * Created by rohitash on 8/3/17.
  */
object ingestorMain extends App {

  val year = args(0)
  val month = args(1)
  val day = args(2)
  val hour = args(3)

  val source = "vdopiasample"

  DateTimeZone.setDefault(DateTimeZone.UTC)
  val t1 = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(0).getMillis
  println(t1)
  val fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val date = fmt.print(t1)

  val query = "select location from ingestor_files where src_name = 'vdopiasample_ReqRes' and `key` in (" + "select `key` from wrangler_files where file_time = '" + date + "' and src_name = '" + source + "') ;"
  println(query)

  val t2 = new DateTime().withTimeAtStartOfDay().withYear(year.toInt).withMonthOfYear(month.toInt).withDayOfMonth(day.toInt).withHourOfDay(hour.toInt).withMinuteOfHour(30).getMillis.toString
  println(t2)

  val halfHourTimestampFunc: (String => String) = (in: String) => {
    val date = DateTime.parse(in, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"))
    //val date = new DateTime(dateLong).withZone(DateTimeZone.UTC) //DateTime.parse(in, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"))
    date.minuteOfHour().get() match{
      case x if x<30 =>
        date.withZone(DateTimeZone.UTC).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0)
          .getMillis.toString
      case _ =>
        date.withZone(DateTimeZone.UTC).withMinuteOfHour(30).withSecondOfMinute(0).withMillisOfSecond(0)
          .getMillis.toString
    }
  }


  import Configs.Sql._
  val fileList = findQuery(query, "location").flatMap(x => x.split(","))
  fileList.foreach(println(_))

  import Configs.SparkConf._
  val data = sqlContext().read.parquet(fileList:_*)
  //data.cache()
  // get 30 min after time in epoch2

  val halfHourTimestampSqlFunc = udf(halfHourTimestampFunc)
  val hourDF = data.withColumn("hour", halfHourTimestampSqlFunc(data("timestamp")))
  val auctions = hourDF.filter("hour <= " + t2).filter("hour >= " + t1).agg(sum("samplingFactor")).collect

  println("auction number in ingestion parquet files : " +  auctions(0).get(0))
  System.exit(0)

}
