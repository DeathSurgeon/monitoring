package Configs

import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{FailoverStrategy, MongoConnectionOptions, MongoDriver}
import reactivemongo.bson.{BSONDocument, BSONDocumentReader}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
/**
  * Created by rohitash on 4/1/17.
  */
object MongoConnection {

  val driver = new MongoDriver()
  val connection1 = driver.connection(ParsedURI(hosts = List(("172.30.0.197",27017)), options = MongoConnectionOptions(nbChannelsPerNode = 30,connectTimeoutMS = 5000),ignoredOptions = List.empty[String], db = None, authenticate = None))
  val strategy = FailoverStrategy(initialDelay = 500 milliseconds,retries = 8,delayFactor = attemptNumber => attemptNumber*1.5)

  val db1 = connection1("ProjectionInfo", strategy)
  val db2 = connection1("ingestion_s3_vdopiasample", strategy)

  val rawCollection : BSONCollection = db1("RawData")
  val groupedSortCollection : BSONCollection = db1("GroupedSort")
  val groupedSortQueueCollection : BSONCollection = db1("GroupedSortQueue")
  val s3ingestion : BSONCollection = db2("s3_summary")

  case class files(file : String)
  implicit object reader extends BSONDocumentReader[files] {
    override def read(bson: BSONDocument): files = {
      files(
        bson.getAs[String]("files").get
      )
    }
  }


}
