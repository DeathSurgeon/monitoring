package Configs

import java.sql.{Connection, DriverManager}

import scala.collection.mutable.ListBuffer

/**
  * Created by rohitash on 3/1/17.
  */
object Sql {
  val url = "jdbc:mysql://172.30.0.197:3306/Ingestion_DM_Vdopia_Sample"
  val driver = "com.mysql.jdbc.Driver"
  val username = "root"
  val password = "sigmoid123"
  var connection:Connection = null


  def findQuery(query:String, param : String) : List[String] = {
    var list = new ListBuffer[String]()
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement
      val rs = statement.executeQuery(query)
      while (rs.next) {
        val parameter = rs.getString(param)
        //println("sql : " + parameter)
        list += parameter
      }
    } catch {
      case e: Exception => e.printStackTrace
    }
    connection.close
    return list.toList

  }

  /*def deleteQuery(query:String) : Unit = {
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement
      val rs = statement.executeUpdate(query)
    } catch {
      case e: Exception => {
        println("while deleting the entry in database")
        e.printStackTrace
      }
    }
    connection.close
  }

  def countQuery(query:String) : Int = {
    var count = 0
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement
      val rs = statement.executeQuery(query)

      while(rs.next()) {
        count =  rs.getInt(1)
        connection.close
        return count
      }

    } catch {
      case e: Exception => e.printStackTrace

    }
    return -1
  }

  def updateQuery(query : String): Unit = {
    var count = 0
    try {
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement
      statement.executeUpdate(query)

    } catch {
      case e: Exception => e.printStackTrace

    }
    return -1

  }*/
}
