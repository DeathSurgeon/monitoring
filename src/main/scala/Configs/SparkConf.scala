package Configs

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.Logger
import org.apache.log4j.Level

/**
  * Created by rohitash on 3/1/17.
  */
object SparkConf {

  val sparkConf = new SparkConf()
    .setAppName("PipelineMonitoring")
    .set("spark.scheduler.mode", "FAIR")
    .set("spark.dynamicAllocation.minExecutors", "0")
    .set("spark.dynamicAllocation.maxExecutors", "10")
    .set("spark.dynamicAllocation.enabled", "true")
    .set("spark.dynamicAllocation.initialExecutors", "3")
    .set("spark.shuffle.service.enabled", "true")
    .set("spark.driver.allowMultipleContexts", "true")
    .set("spark.dynamicAllocation.executorIdleTimeout", "60s")
    .set("spark.cleaner.ttl", "1800s")

  val rootLogger = Logger.getRootLogger()
  rootLogger.setLevel(Level.ERROR)

  val localConfig = new SparkConf().setMaster("local[*]")
  val numPartitions = 4
  /****************/


  var currentActiveSparkContext:SparkContext = new SparkContext(sparkConf)

  var currentActiveSQLContext:SQLContext = new SQLContext(currentActiveSparkContext)

  def sqlContext() = currentActiveSQLContext
  def sparkContext() = currentActiveSparkContext
}
