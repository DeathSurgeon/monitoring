name := "Monitoring"

version := "1.0"

scalaVersion := "2.10.4"

//resolvers += "Cloudera Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.18"
libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.1" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.6.1" % "provided"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5"


libraryDependencies ++= {
  Seq(
    "org.reactivemongo" %% "reactivemongo" % "0.11.10",
    "joda-time" % "joda-time" % "2.9.4"
  )
}

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.10"
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.10"

assemblyMergeStrategy in assembly := {
  case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case PathList(ps@_*) if ps.last endsWith ".class" => MergeStrategy.first
  case "application.conf" => MergeStrategy.concat
  case "unwanted.txt" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}


    